import React, { Component } from "react";
import { connect } from "react-redux";
import { addCommentFetchAPI, getOrder, updateOrder, updateStatusOrder } from "../../actions";
import jwt_decode from "jwt-decode";
import { withRouter } from "react-router";
import FormatCash from "../../utils/formatCurrency";
import Swal from "sweetalert2";
import { Button, Modal } from 'react-bootstrap'
import { FormGroup,Form, Input, Label } from "reactstrap";
import starRatings from "react-star-ratings/build/star-ratings";
import StarRatings from "react-star-ratings";

class MyOrder extends Component {

  constructor(props) {
    super(props);
    this.state = {
        show: false,
        userId:"",
        rating: 0,
        productIdFocus:"",
        orderIdUpdate:"",
    }
}

  componentDidMount() {
    const { isLoggedIn } = this.props
    var decoded = null;
    var userId = null;
    if (isLoggedIn.isLoggedIn === true) {
      const { user } = isLoggedIn;
      if (user !== null) {
        decoded = jwt_decode(user.token);
        userId = decoded.id;
        this.setState({
          userId:userId
      })
        this.props.fetchAllOrder(userId)
      }
    } else {
      this.props.history.push('/auth/login')
    }
  }

  handelsubmit = (event) => {
    event.preventDefault();
    let decoded = null;
    let userId = null;
    decoded = jwt_decode(this.props.isLoggedIn.user.token);
    let token=this.props.isLoggedIn.user.token;
    userId = decoded.id;
    const comment = {
      context: event.target.comment.value,
      productId: this.state.productIdFocus,
      user_Id: this.state.userId,
      rate:this.state.rating
    }
    console.log(comment)
    this.setState({
      show:false
    })
    const order= {
      productId:this.state.productIdFocus,
      userId: this.state.userId,
      orderId:this.state.orderIdUpdate,
      status:"paid"
    }
    this.props.onaddComment(comment, token);
    this.props.updateOrder(order);
  }

  changeRating =(newRating)=> {
    this.setState({
      rating: newRating
    });
  }

  onCancel = (product) => {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async (result) => {
      if (result.isConfirmed) {
        let decoded = null;
        let userId = null;
        decoded = jwt_decode(this.props.isLoggedIn.user.token);
        userId = decoded.id;
        await this.props.deleteOrder(product);
        Swal.fire(
          'Deleted!',
          'Your order has been deleted.',
          'success'
        )
        await this.props.fetchAllOrder(userId)
      }
    })
  }

  onRateProduct=(product) => {
    this.setState({
      show: true,
      productIdFocus:product.productId,
      orderIdUpdate: product.orderId
    })
  }

  handleClose=()=> {
    this.setState({
      show: false
    })
  }

  render() {
    const { order } = this.props
    return (
      <div className="list-product-of-cart" style={{ marginTop: '0!important' }}>
        <div className="list-content mx-auto">
          <div style={{ marginBottom: '30px' }} className="title" ><h5>DANH SÁCH ĐƠN HÀNG</h5></div>
          <ul>
            {
              order ? order.map((product, index) => (
                <li key={index} id={1 + 1}>
                  <div className="item-cart" idproductincart={3}>
                    <div className="infor-mall">
                      <div className="skin">
                        <div className="name-of-mall">{product.product_name}</div>
                      </div>
                      <div className="btn-delete-product-from-cart">
                        {/* <button title="xóa sản phẩm" onClick={() => this.handleDelete(product)} ><i className="fa fa-trash" /></button> */}
                      </div>
                    </div>
                    <img className="img-product" style={{ backgroundImage: `../../images/` + product.images }} src={product.images}>
                    </img>
                    <div className="quick-infor">
                      <div className="name">{ }</div>
                      <div className="code-color-selected">
                        <span className="label">Ngày đặt hàng:</span><span className="value" idcolor="123abc">{product.createdAt}</span>
                      </div>
                      <div className="size-selected">
                        <span className="label">Giá:</span><span className="value" idsize="10 lít"><FormatCash str={String(product.price)} /> đ</span>
                      </div>
                      <div className="qty-selected">
                        <span className="label">Số lượng:</span><span className="value" qtyproduct={1}>{product.quantity}</span>
                      </div>
                      <div className="qty-selected">
                        <span className="label">Trạng thái:</span><span className="value" qtyproduct={1}>{product.status}</span>
                      </div>
                    </div>
                    <div className="price-product">
                      <div className="price">
                        <span><FormatCash str={String(product.quantity * product.price)} /></span><span style={{ textDecorationLine: 'underline' }}>đ</span>
                      </div>
                    </div>
                    {product?.status === 'pending' && (
                      <div className="col-btn-right">
                        <div className="btn-change-option-product">
                          <button onClick={() => this.onCancel(product)}>Hủy đơn</button>
                        </div>
                      </div>
                    )}
                    {product?.status === 'shipped' && (
                      <div className="col-btn-right">
                        <div className="btn-change-option-product">
                          <button onClick={() => this.onRateProduct(product)}>Đánh giá</button>
                        </div>
                        <Modal
                        show={this.state.show}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title style={{color:"green",marginLeft:"135px"}}>Đánh giá sản phẩm</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form onSubmit={this.handelsubmit}>
                                <FormGroup>
                                    <Label for="address">Bình luận về sản phẩm</Label>
                                    <Input
                                        type="text"
                                        name="comment"
                                        id="comment"
                                        defaultValue=''
                                    />
                                </FormGroup>
                                <FormGroup style={{display:"flex",justifyContent: "center"}}>
                                <StarRatings rating={this.state.rating}
                                    starRatedColor="blue"
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name='rating'/>
                                </FormGroup>
                                <FormGroup>
                                    <Button color="primary" onClick={this.handleClose} style={{marginRight:'25px'}}>
                                        Hủy bỏ
                                    </Button>
                                    <Button color="primary" type="submit">
                                        Đánh giá
                                    </Button>
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                    </Modal>
                      </div>
                    )}
                  </div>
                </li>
              )) : <>Không có đơn hàng</>
            }
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    order: state.order,
    isLoggedIn: state.users,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllOrder: (userId) => {
      dispatch(getOrder(userId));
    },
    deleteOrder: (product) => {
      dispatch(updateOrder(product));
    },
    onaddComment: (comment, token) => {
      dispatch(addCommentFetchAPI(comment, token))
    },
    updateOrder: (order) => {
      dispatch(updateStatusOrder(order));
    },
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyOrder));
