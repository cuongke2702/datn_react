import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                  <div>
        {/* tab smooth scrolling page */}
        <div className="smooth-scrolling-page-card">
          <ul className="list">
            <a href="#section-1">
              <li className="item"><i className="fa fa-history" /></li>
              <div className="title-hover-on-tab" style={{top: '10%'}}>
                Tìm kiếm phổ biến
              </div>
            </a>
            <a href="#section-2">
              <li className="item"><i className="fa fa-shopping-bag" /></li>
              <div className="title-hover-on-tab" style={{top: '28%'}}>
                Trung tâm mua sắm
              </div>
            </a>
         
            <a href="#section-4">
              <li className="item"><i className="fa fa-th-list" /></li>
              <div className="title-hover-on-tab" style={{top: '65%'}}>
                Danh mục ngành hàng
              </div>
            </a>
            <a href="#section-5">
              <li className="item"><i className="fa fa-user" /></li>
              <div className="title-hover-on-tab" style={{top: '83%'}}>
                Dành riêng cho bạn
              </div>
            </a>			
          </ul>
        </div>
        {/* footer */}
        <div className="footer">
          <div className="contact">LIÊN HỆ VỚI VUNGCAOFOOD</div>
          <div className="sale-contact">NHU CẦU BÁN HÀNG</div>
        </div>
      </div>
            </div>
        );
    }
}

export default Footer;