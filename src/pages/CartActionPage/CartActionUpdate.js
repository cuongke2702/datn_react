import React, { Component } from 'react';

class CartActionUpdate extends Component {
    render() {
        return (
            <div>
        <div className="layer-black-behind-box-change">
        <div className="box-change-info-product" >
          <div className="img-product-in-box col-lg-6 col-ms-12 col-md-12 col-12">
            <div className="bg-img mx-auto" style={{backgroundImage: 'url(images/na-đài-loan.jpg)'}} />
          </div>
          <div className="infor col-lg-6 col-ms-12 col-md-12 col-12">
            <div className="name">
              [na đài loan] nhìn là biết đéo ngon rồi nói chi ăn haixx chán 
            </div>
            <div className="bar-code-color">
              <span className="label float-left">Màu sắc</span>
              <div className="list-option-code-color">
                <ul>
                  <li>
                    <div className="option-code-color">đỏ</div>
                  </li>
                  <li>
                    <div className="option-code-color">xanh</div>
                  </li>
                  <li>
                    <div className="option-code-color">đen</div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="bar-size">
              <span className="label float-left">Kích thước</span>
              <div className="list-option-size">
                <ul>
                  <li>
                    <div className="option-size">1l</div>
                  </li>
                  <li>
                    <div className="option-size">2l</div>
                  </li>
                  <li>
                    <div className="option-size">3l</div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="bar-num-p">
              <span>
                <span className="label float-left">Số lượng</span>
                <span>
                  <button type="button" id="btn-sub-num-p" style={{cursor: 'not-allowed', color: 'gray'}}><i className="fa fa-minus" style={{fontSize: '150%'}} /></button>
                </span>
                <span className="num-p" style={{color: '#095764', fontSize: '150%', width: '20px', textAlign: 'center', fontWeight: 'bold'}}>1</span>
                <span>
                  <button type="button" id="btn-add-num-p" style={{cursor: 'pointer'}}><i className="fa fa-plus" style={{fontSize: '150%'}} /></button>
                </span>
              </span>
            </div>
            <div className="sum-price">
              <span className="label float-left">Tổng cộng</span>
              <span className="price">120.000 đ</span>
            </div>
            <div className="btn-save-change">
              <button type="button">XONG</button>
            </div>
          </div>
        </div>
        <div className="btn-cancel">
          <button><i className="fa fa-close" /></button>
        </div>
      </div>
            </div>
        );
    }
}

export default CartActionUpdate;