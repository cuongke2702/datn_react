import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { addThanhToan } from '../../actions';
import FormatCash from '../../utils/formatCurrency';



class CheckOutAction extends Component {

  constructor(props) {
    super(props);
    this.state = {
      product: {},
    };
  }



  componentDidMount() {
    const { cart, match, isLoggedIn } = this.props;

    let product = null;
    if (match) {
      const { cartId } = match.params;
      const buy = JSON.parse(localStorage.getItem('buy'));
      if (buy) {
        this.setState({
          product: buy,
        })
      } else {
        product = cart.find(each => each.cartId.toString() === cartId.toString())
        this.setState({
          product: product,
        })
      }
    }
  }

  promiseState(p) {
    const t = {};
    return Promise.race([p, t])
      .then(v => (v === t) ? "pending" : "fulfilled", () => "rejected");
  };

  thanhtoan12 = async (product) => {
    const result = await this.props.onthanhtoan(product);
    let status = 'pending'
    await this.promiseState(result).then(state => status = state);
    if (status === 'fulfilled') {
      if (!localStorage.getItem('buy') || JSON.parse(localStorage.getItem('buy'))?.length === 0) {
        const newCart = JSON.parse(localStorage.getItem('cart')).filter(item => item.productId !== product.productId);
        localStorage.setItem('cart', JSON.stringify(newCart));
      }
      localStorage.removeItem('buy')
      this.props.history.push('/order')
    }
  }
  render() {
    const { product } = this.state
    return (
      <div>
        <div className="col-right col-lg-4 col-md-4 col-12">
          <div className="content">
            <div className="bar-to-pay-btn ">
              <button type="button" className="bg-button-enable">HÓA ĐƠN</button>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Tạm tính</div><div className="value" id="price-id-add-1"><FormatCash str={String(product.price * product.quantity)} /> đ</div>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Phí giao hàng</div><div className="value" id="price-id-add-2" />
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Giảm phí giao hàng</div><div className="value" id="price-id-sub-1"><span>-</span>0</div>
            </div>
            <div className="row-checkout-of-col-right row-checkout-of-col-right-show">
              <div className="input-discount-code">
                <input disabled={true} readOnly type="text" name="txt-discount-code" placeholder="Nhập mã giảm giá" />
              </div>
              <div className="btn-apply-code">
                <button type="button">ÁP DỤNG</button>
              </div>
            </div>
            <div className="row-checkout-of-col-right row-checkout-of-col-right-hide">
              <div className="row-label">Giảm giá</div><div className="value" id="price-id-sub-2"><span>-</span>20.000</div>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Tổng</div><div className="value" id="summany" >
                <FormatCash str={String(product?.price * product?.quantity)} /> đ</div>
            </div>
            <div className="bar-to-pay-btn ">
              <button type="button" onClick={() => this.thanhtoan12(product)} className="bg-button-enable">THANH TOÁN</button>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     isLoggedIn : state.users,
//     cart:state.cart,
//   };
// };
const mapDispatchToProps = (dispatch, props) => {
  return {
    onthanhtoan: async (product) => {
      return await dispatch(addThanhToan(product));
    },
  };
};

export default withRouter(connect(null, mapDispatchToProps)(CheckOutAction));