import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actGetProductsRequest, actGetComment, addCommentFetchAPI, addCat, actGetProductByCatId, actGetProductsByCatIdRequest, actGetRelateProductsRequest, getProductAIs, getUserById } from '../../actions';
import jwt_decode from "jwt-decode";
import ProductHomeCat from '../../components/ProductItem/ProductHomeCat';
import ProductItem from '../../components/ProductList/ProductItem';
import FormatCash from '../../utils/formatCurrency';
import StarRatings from 'react-star-ratings';
import ReactPaginate from 'react-paginate';

class ReviewProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productId: "",
      productName: "",
      images: "",
      price: "",
      quantity: "",
      detail: "",
      userId: '',
      cartId: '',
      rateAvg:'',
      rating: 0,
      numberComent:0,
      comments: [],
      comment: {
        context: '',
        productId: '',
        user_Id: '',
        username: '',
        rate:'',
      },
      product: {
        cartId: "",
        userId: "",
        productId: "",
        productName: "",
        quantity: 1,
        price: "",
        image: ''
      },
      register:"",
      relateProduct: [],
    };
    const myRef = React.createRef(null);
  }
  componentDidMount() {
    var { match } = this.props;
    if (match) {
      var productId = match.params.productId;
      this.props.onGetProduct(productId);
      this.props.onGetComment(productId,this.state.numberComent);
      this.props.getProductAI(productId)
    }
    const { isLoggedIn } = this.props
    var decoded = null;
    var userId = null;
    if (isLoggedIn != null) {
      const { user } = isLoggedIn;
      if (user !== null) {
        decoded = jwt_decode(user.token);
        userId = decoded.id;
        this.props.getUser(userId)
        this.props.getRelateProduct(userId)
      }
    }
    this.setState({
      userId: userId
    })
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.itemEditing !== this.props.itemEditing) {
      const { itemEditing } = this.props;
      this.setState({
        productId: itemEditing.productId,
        productName: itemEditing.productName,
        images: itemEditing.images,
        price: itemEditing.price,
        quantity: itemEditing.quantity,
        detail: itemEditing.detail,
        rateAvg: Math.floor(itemEditing.rateAvg),
        product: {
          productId: itemEditing.productId,
          productName: itemEditing.productName,
          price: itemEditing.price,
          quantity: 1,
          userId: this.state.userId,
          image: itemEditing.images,
          rateAvg: Math.floor(itemEditing.rateAvg)
        }
      });
    }
    if (prevProps.comments !== this.props.comments) {
      const { comments } = this.props
      this.setState({
        comments: comments
      })
    }
    if (prevProps.register !== this.props.register) {
      const { register } = this.props
      this.setState({
        register: register
      })
    }
    if (prevProps.products !== this.props.products) {
      const { products } = this.props
      this.setState({
        relateProduct: products
      })
    }
    if (prevProps.match.params.productId !== this.props.match.params.productId) {
      this.props.onGetProduct(this.props.match.params.productId);
    }
    
  }

  addComment = (event) => {
    event.preventDefault();
    const user = JSON.parse(localStorage.getItem("user"));
    if (user !== null) {
      var context = event.target.context.value;
      const comment = {
        context: context,
        productId: this.state.productId,
        user_Id: this.state.userId,
        fullName:this.state.register.fullName,
        rate:this.state.rating
      }
      this.props.onaddComment(comment, user.token);
      event.target.context.value = "";
    } else {
      alert('Vui lòng đăng nhập để thực hiện');
      this.props.history.push('/auth/login');
    }
  };

  changeRating =(newRating, name)=> {
    this.setState({
      rating: newRating
    });
  }
  

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  };

  handleChangess = (event) => {
    this.setState({
      product: {
        productId: this.state.productId,
        productName: this.state.productName,
        price: this.state.price,
        quantity: event.target.value,
        userId: this.state.userId,
        image: this.state.image,
        rateAvg: Math.floor(this.state.rateAvg)
      }
    });
  };

  onHandleComment =(productId,numberComent)=> {
    
    this.props.onGetComment(productId,numberComent);
  }

  hanldeBuy = () => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user !== null) {
      localStorage.setItem('buy', JSON.stringify(this.state.product));
      this.props.history.push(`/checkout/${this.state.product.productId}`);
    } else {
      alert('vui lòng đăng nhập để mua hàng!');
      this.props.history.push('/auth/login');
    }
  }

  scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

  onClickItem = () => {
    this.scrollToRef(this.myRef)
  }

  render() {
    var { productName, images, price, quantity, detail,rateAvg, product } = this.state;

    const { relateProduct } = this.state
    var rateAvgFlorr=Math.floor(rateAvg)

    var { comments } = this.state;
    const {register} =this.state
    const cart = JSON.parse(localStorage.getItem("cart"));
    return (
      <div>
        {/* thông tin sản phẩm */}
        <div ref={this.myRef} className="container-detail-product">
          <form method="get" action="checkout.html">
            <div className="detail-product mx-auto">
              <div className="img-product-review col-lg-4 col-md-12 col-sm-12 float-left">
                {/* <div className="img-selected mx-auto" style={{backgroundImage: 'url(images/dau-rung.jpg)'}}>
              </div> */}

                <img className="img-selected mx-auto" src={images} alt="" />


                <div className="other-img">
                  <ul>
                    <li className="img-clicked">
                      <div className="item" style={{ backgroundImage: 'url(../../images/tao-do.jpg)' }}>
                      </div>
                    </li>
                    <li>
                      <div className="item" style={{ backgroundImage: 'url(../../images/tao-do.jpg)' }}>
                      </div>
                    </li>
                    <li>
                      <div className="item" style={{ backgroundImage: 'url(../../images/tao-do.jpg)' }}>
                      </div>
                    </li>
                    <li>
                      <div className="item" style={{ backgroundImage: 'url(../../images/na-đài-loan.jpg)' }}>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="infor-product-review col-lg-8 col-md-12 col-sm-12 float-left">
                <div className="content-infor-product">
                  {/* dòng hiển thị tiêu đề , tên sản phẩm , giới thiệu ..... */}
                  <div className="describe-product">
                    <h3>{productName}</h3>
                  </div>
                  {/* dòng hiển thị thông tin đánh giá sản phẩm */}
                  <div className="vote-star" style={{ width: '95%', height: '25px' }}>
                    <div className="stars">
                      <span className="float-left" style={{ color: '#53a6eb', marginRight: '20px' }}>Đánh giá:</span>
                      <span>
                        <ul>
                          {
                            Array.from(Array(rateAvgFlorr), (e, i) => {
                              return <li>
                              <i className="list-star-item fa fa-star star-select" />
                                    </li>
                            })
                          }
                           {
                      Array.from(Array(5-rateAvgFlorr), (e, i) => {
                        return <li>
                        <i className="list-star-item fa fa-star" />
                      </li>
                      })
                    }
                        </ul>
                      </span>
                      <span className="total-vote float-left"><h6>({quantity})</h6></span>
                      <span className="float-right">
                        <span>
                          <button className="share-btn btn-normal" type="button" name="share-btn" title="chia sẻ">
                            <i className="fa fa-share" />
                          </button>
                        </span>
                        <span>
                          <button className="vote-star-btn btn-normal" type="button" name="vote-star-btn" title="đánh giá">
                            <i className="fa fa-heart" />
                          </button>
                        </span>
                      </span>
                    </div>
                  </div>
                  {/* dong hiển thị thông tin thương hiệu */}
                  <div className="trademark-product float-left" style={{ width: '100%', height: '50px', borderBottom: '1px solid orange' }}>
                    <span style={{ marginRight: '10px', color: '#53a6eb' }}>Chi tiết: </span>
                    <span style={{ borderRight: '1px solid orange', paddingRight: '10px' }}><a href>{}</a></span>
                  </div>
                  {/* dòng hiển thị giá sản phẩm */}
                  <div className="price-product">
                    <h1 style={{ color: 'orange' }}><FormatCash str={String(price) || '0000'} /> <span style={{ textDecorationLine: 'underline', marginLeft: '2px', fontSize: '90%' }}>đ</span></h1>
                  </div>
                  {/* dòng hiển thị giá cũ và số tiền tiết kiệm dc */}
                  <div className="sale-price" style={{ marginBottom: '25px' }}>
                    <h6><span style={{ marginRight: '10px', color: 'gray' }}><strike><FormatCash str={String(price) || '0000'} /></strike></span><span style={{ textDecoration: 'underline', fontSize: '90%', color: 'gray' }}>đ</span><span style={{ marginLeft: '10px' }}>-10%</span> </h6>
                  </div>
                  <div className="bar-code-colors" style={{ marginTop: '20px' }}>
                    <span className="float-left" style={{ paddingRight: '20px', color: 'gray' }}></span>
                    <div className="list-code-color">
                    </div>
                  </div>
                  {/* dòng hiển thị tùy chọn số lượng hàng */}
                  <div className="bar-size float-left" style={{ marginTop: '20px' }}>
                    <span className="float-left" style={{ paddingRight: '20px', color: 'gray' }}>Số sản phẩm trong kho</span>
                    <div className="list-size">
                      {
                        quantity
                      }
                    </div>
                  </div>
                  {/* dòng hiển thị thanh tùy chọn số lượng và kích cỡ màu sắc */}
                  <div className="bar-option-num-product float-left" style={{ width: '100%', height: '40px', lineHeight: '40px' }}>
                    <span>
                      <span style={{ marginRight: '20px', color: 'gray' }}>Số lượng</span>
                      <input
                        type="number"
                        min={0}
                        name="quantitygiohang"
                        value={this.state.product.quantity}
                        onChange={this.handleChangess}
                        className="num-p" id="quantitygiohang"
                        style={{
                          color: '#095764',
                          fontSize: '15px',
                          width: '50px',
                          textAlign: 'center',
                          fontWeight: 'bold',
                          margin: '10px 0',
                          height: '30px',
                          outline: 'none'
                        }}
                      />
                    </span>
                  </div>
                  {/* dòng hiển thị nút thêm vào giỏ hàng và mua hàng */}
                  <div className="btn-add-and-buy float-left" style={{ width: '100%', height: '50px' }}>
                    <span>
                      <button type="button" className="btn-add-into-cart btn-large-size" style={{ height: '100%', backgroundColor: 'darkorange', color: 'white', borderRadius: '5px' }}
                        onClick={() => this.onAddToCart(this.state.product)}  >THÊM VÀO GIỎ HÀNG</button>
                    </span>
                    {/* phần thông báo đã thêm sản phẩm vào giỏ hàng  */}
                    <span>
                      <button type="button" onClick={() => this.hanldeBuy()} className="btn-buy-now btn-large-size" style={{ height: '100%', backgroundColor: 'orange', color: 'white', borderRadius: '5px' }}>MUA NGAY</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="container-description col-lg-10 col-md-10 col-sm-12 col-12">
        <div className="container-description col-lg-10 col-md-10 col-sm-12 col-12 mota">
          <div className='detail'>MÔ TẢ SẢN PHẨM</div>
          <div class="Fo12Im">
            <div class="Mhqp_x">
            <p className='textDetai'>{detail}</p></div></div></div>
        </div>
        <div className="container-description col-lg-10 col-md-10 col-sm-12 col-12">
          <div className="description-content ">
            <ul className=" ul-item mx-auto">
              <li className="li-item">
                <div className="tab-description  show-tab">
                  <div className="comment-product">

                    <div className="comment-content">
                      <div className="question-about-product">
                        <div>
                          <h3>Đánh giá sản phẩm</h3>
                        </div>
                        <div className="list-question" style={{ width: '100%' }}>
                          <ul>
                            {comments ? comments.map(comment => (
                              <li key={comment.commentId} className="question-item">
                                <div className="question">
                                  <div className="infor-user">
                                    <span className="icon"><i className="fa fa-comment" /></span>
                                    <span className="user-name">{comment.fullName}</span>
                                    <span className="action">{comment?.createdAt}</span>
                                    <div className="stars" style={{float:'right',marginRight:"20px"}}>
                                      <ul>
                                        {
                                           comment.rate==null?
                                           <></>:
                                          Array.from(Array(comment.rate), (e, i) => {
                                            return <li>
                                            <i className="list-star-item fa fa-star star-select" />
                                          </li>
                                          })
                                        }
                                        {
                                          comment.rate==null?
                                          <></>
                                         :
                                          Array.from(Array(5-comment.rate), (e, i) => {
                                            return <li>
                                            <i className="list-star-item fa fa-star" />
                                          </li>
                                          })
                                        }
                                      </ul>
                                  </div>
                                  </div>
                                  
                                  <div className="content" id="content">
                                    {comment.context}
                                  </div>
                                </div>
                              </li>
                            )) : <></>
                            }
                          </ul>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </li>
            </ul>
          </div>
          {/* {
            comments.length>=3?
            <div className='comment-page'>
            <h4><a onClick={this.onHandleComment(this.state.productId,this.state.numberComent)}>Hiển thị thêm bình luận {}</a></h4>
          </div>:
          <></>
          } */}
          <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        //onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        pageCount={3}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        containerClassName="pagination justify-content-center"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        activeClassName="active"
      />
        </div>
        {
          JSON.parse(localStorage.getItem("user"))!=null?
          <div className="list-product-result">
          <div className="title">
            <h3>Sản phẩm liên quan</h3>
          </div>
          <div className="content mx-auto">
            {
              relateProduct.map((product, index) => (
                <ProductItem onClick={() => this.onClickItem()} key={index} product={product} />
              ))
            }
          </div>
        </div>:
        <></>
        }
        

      </div>

    );
  }
  onAddToCart = (product) => {
    const user = JSON.parse(localStorage.getItem("user"));
    const cart = JSON.parse(localStorage.getItem("cart"));
    var cartId = 0;
    if (cart !== null) {
      cartId = tudongtangCart(cart.length)
    }
    var products = {
      cartId: cartId,
      productId: product.productId,
      productName: product.productName,
      price: product.price,
      quantity: product.quantity,
      userId: product.userId,
      image: this.state.images
    }

    if (user !== null) {
      this.props.onAddCartFect(products);
    } else {
      alert('vui lòng đăng nhập để mua hàng!');
      this.props.history.push('/auth/login');
    }
  }
}

const mapStateToProps = (state) => {
  return {
    itemEditing: state.itemEditing,
    comments: state.comments,
    isLoggedIn: state.users,
    products: state.products,
    register:state.register
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetProduct: (productId) => {
      dispatch(actGetProductsRequest(productId));
    },
    onGetComment: (productId,numberComment) => {
      dispatch(actGetComment(productId,numberComment))
    },
    onaddComment: (comment, token) => {
      dispatch(addCommentFetchAPI(comment, token))
    },
    onAddCartFect: (product) => {
      dispatch(addCat(product))
    },
    getRelateProduct: (userId) => {
      dispatch(actGetRelateProductsRequest(userId))
    },
    getProductAI: (productId) => {
      dispatch(getProductAIs(productId))
    },
    getUser: (userId) => {
      dispatch(getUserById(userId))
    }
  };
};

var tudongtangCart = (cartId) => {
  var cartNumber = 0;
  cartNumber = cartId + 1;
  return cartNumber;
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewProductPage);
