import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal } from 'react-bootstrap'
import DataGrid from 'react-data-grid';
import { FormGroup,Form, Input, Label } from "reactstrap";
import { getOrder, getUserById, updateOrder, updateProfile } from "../../actions";
import jwt_decode from "jwt-decode";
import { withRouter } from "react-router";

class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            showPassWord:false,
            userId:"",
        }
    }

    
    handleShow = () => {
        this.setState({
            show: true
        })
    }
    handleShowChangePassword = () => {
        this.setState({
            showPassWord: true
        })
    }
    handleCloseChangePassword = () => {
        this.setState({
            showPassWord: false
        })
    }
    handleClose = () => {
        this.setState({
            show: false
        })
    }
    handelsubmit = (event) => {
        event.preventDefault();
        this.setState({
            show:false
        })
        const user = {
          userId: this.state.userId,
          fullName: event.target.fullName.value,
          phone: event.target.phone.value,
          address: event.target.address.value,
          email: event.target.email.value,
        }
        this.props.onUpdateUser(user);
      }

    componentDidMount() {
        const { isLoggedIn } = this.props
        var decoded = null;
        var userId = null;
        if (isLoggedIn.isLoggedIn === true) {
            const { user } = isLoggedIn;
            if (user !== null) {
                decoded = jwt_decode(user.token);
                userId = decoded.id;
                this.setState({
                    userId:userId
                })
                this.props.fectchUser(userId)
            }
        } else {
            this.props.history.push('/auth/login')
        }
    }

    render() {
        const { data } = this.props
        console.log(data);

        var columns = [
            { key: 'title', name: 'Thông tin'},
            { key: 'data', name: 'data' }
          ];
          
         var rows = [
            { title: 'Họ và Tên', data: data.fullName },
            { title: 'Số điện thoại ', data: data.phone },
            { title: 'Tên tài khoản', data: data.userName },
            { title: 'Email', data: data.email },
            { title: 'Địa Chỉ ', data: data.address },
            { title: 'Lịch sự mua hàng ', data: <a>assss</a> },
          ];
    
        return (

            <div className="list-product-of-cart" style={{ marginTop: '0!important' }}>
                <div className="list-content mx-auto" >
                    <div style={{ marginBottom: '30px' }} className="title" ><h5>THÔNG TIN CÁ NHÂN</h5></div>
                    <div><a className="nav-link" href="/purchasehistory">
                    Lịch sử mua hàng
                  </a></div>
                    <DataGrid
                    rows={rows}
                    columns={columns}
                    />
                    <div className="button-edit">
                    <Button variant="primary" onClick={this.handleShow}>
                        Chỉnh sửa
                    </Button>
                    <Modal
                        show={this.state.show}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Modal heading</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form onSubmit={this.handelsubmit}>
                                <FormGroup>
                                    <Label for="address">Full name</Label>
                                    <Input
                                        type="text"
                                        name="fullName"
                                        id="fullName"
                                        defaultValue={data.fullName}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="address">Phone</Label>
                                    <Input
                                        type="text"
                                        name="phone"
                                        id="phone"
                                        defaultValue={data.phone}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="address">address</Label>
                                    <Input
                                        type="text"
                                        name="address"
                                        id="address"
                                        defaultValue={data.address}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="address">Email</Label>
                                    <Input
                                        type="text"
                                        name="email"
                                        id="email"
                                        defaultValue={data.email}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Button color="primary" onClick={this.handleClose}>
                                        close
                                    </Button>
                                    <Button color="primary" type="submit">
                                        Save
                                    </Button>
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                    </Modal>
                    </div>

                    <div className="button-changepass">
                    <Button variant="primary" onClick={this.handleShowChangePassword}>
                        Đổi mật khẩu
                    </Button>
                    <Modal
                        show={this.state.showPassWord}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        onHide={this.handleCloseChangePassword}>
                        <Modal.Header closeButton>
                            <Modal.Title>Đổi mật khẩu</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form onSubmit={this.handelsubmit}>
                                <FormGroup>
                                    <Label for="address">Mật khẩu hiện tại</Label>
                                    <Input
                                        type="text"
                                        name="fullName"
                                        id="fullName"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="address">Mật khẩu mới</Label>
                                    <Input
                                        type="text"
                                        name="phone"
                                        id="phone"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="address">Xác nhận lại mật khẩu</Label>
                                    <Input
                                        type="text"
                                        name="address"
                                        id="address"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Button color="primary" type="submit">
                                        Save
                                    </Button>
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                    </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.register,
        isLoggedIn: state.users,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fectchUser: (userId) => {
            dispatch(getUserById(userId));
        },
        onUpdateUser: (user) => {
            dispatch(updateProfile(user));
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyProfile));
