import React, { Component } from "react";
import { connect } from "react-redux";
import { addCommentFetchAPI, getOrder, getOrders, updateOrder, updateStatusOrder } from "../../actions";
import jwt_decode from "jwt-decode";
import { withRouter } from "react-router";
import FormatCash from "../../utils/formatCurrency";
import Swal from "sweetalert2";
import { Button, Modal } from 'react-bootstrap'
import { FormGroup,Form, Input, Label } from "reactstrap";
import starRatings from "react-star-ratings/build/star-ratings";
import StarRatings from "react-star-ratings";

class PurchaseHistory extends Component {

  constructor(props) {
    super(props);
    this.state = {
        show: false,
        userId:"",
        rating: 0,
        productIdFocus:"",
        orderIdUpdate:"",
    }
}

  componentDidMount() {
    const { isLoggedIn } = this.props
    var decoded = null;
    var userId = null;
    if (isLoggedIn.isLoggedIn === true) {
      const { user } = isLoggedIn;
      if (user !== null) {
        decoded = jwt_decode(user.token);
        userId = decoded.id;
        this.setState({
          userId:userId
      })
        this.props.fetchAllOrder(userId)
      }
    } else {
      this.props.history.push('/auth/login')
    }
  }
 
  render() {
    const { order } = this.props
    return (
      <div className="list-product-of-cart" style={{ marginTop: '0!important' }}>
        <div className="list-content mx-auto">
          <div style={{ marginBottom: '30px' }} className="title" ><h5>Lịch sử mua hàng</h5></div>
          <ul>
            {
              order ? order.map((product, index) => (
                <li key={index} id={1 + 1}>
                  <div className="item-cart" idproductincart={3}>
                    <div className="infor-mall">
                      <div className="skin">
                        <div className="name-of-mall">{product.product_name}</div>
                      </div>
                      <div className="btn-delete-product-from-cart">
                        {/* <button title="xóa sản phẩm" onClick={() => this.handleDelete(product)} ><i className="fa fa-trash" /></button> */}
                      </div>
                    </div>
                    <img className="img-product" style={{ backgroundImage: `../../images/` + product.images }} src={`../../images/` + product.images}>
                    </img>
                    <div className="quick-infor">
                      <div className="name">{ }</div>
                      <div className="code-color-selected">
                        <span className="label">Ngày đặt hàng:</span><span className="value" idcolor="123abc">{product.createdAt}</span>
                      </div>
                      <div className="size-selected">
                        <span className="label">Giá:</span><span className="value" idsize="10 lít"><FormatCash str={String(product.price)} /> đ</span>
                      </div>
                      <div className="qty-selected">
                        <span className="label">Số lượng:</span><span className="value" qtyproduct={1}>{product.quantity}</span>
                      </div>
                      <div className="qty-selected">
                        <span className="label">Trạng thái:</span><span className="value" qtyproduct={1}>{product.status}</span>
                      </div>
                    </div>
                    <div className="price-product">
                      <div className="price">
                        <span><FormatCash str={String(product.quantity * product.price)} /></span><span style={{ textDecorationLine: 'underline' }}>đ</span>
                      </div>
                    </div>
                  </div>
                </li>
              )) : <>No data</>
            }
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    order: state.order,
    isLoggedIn: state.users,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllOrder: (userId) => {
      dispatch(getOrders(userId));
    },
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PurchaseHistory));
