import React from 'react';
import { connect } from 'react-redux';
import Cart from './components/Cart/Cart';
import CheckOut from './components/CheckOut/CheckOut';
import Login from './components/Login/Login';
import ProductListByCat from './components/ProductList/ProductByCat';
import Register from './components/Register/Register';
import HomePage from './pages/HomePage/HomePage';
import MyOrder from './pages/MyOrder/MyOrder';
import MyProfile from './pages/Profile/Profile';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage'
import ReviewProductPage from './pages/ReviewProductPage/ReviewProductPage';
import cart from './reducers/cart';
import PurchaseHistory from './pages/Profile/PurchaseHistory';


const routes = [
    {
        path: '/home',
        exact: true,
        main: () => <HomePage />
    },

    {
        path: '/cart',
        exact: true,
        main: () => <Cart />
    },
    {
        path: '/checkout/:cartId',
        exact: true,
        main: (match) => <CheckOut match={match} />
    },
    {
        path: '/product-byCat/:catId',
        exact: false,
        main: ({ match, history }) => <ProductListByCat match={match} history={history} />
    },
    {
        path: '/product-detail/:productId',
        exact: false,
        main: ({ match, history }) => <ReviewProductPage match={match} history={history} />
    },
    {
        path: '/auth/login',
        exact: false,
        main: () => <Login />
    },
    {
        path: '/auth/register',
        exact: false,
        main: () => <Register />
    },

    {
        path: '/order',
        exact: true,
        main: () => <MyOrder />
    },
    {
        path: '/profile',
        exact: true,
        main: () => <MyProfile />
    },
    {
        path: '/purchasehistory',
        exact: true,
        main: () => <PurchaseHistory/>
    },

    {
        path: '',
        exact: false,
        main: () => <HomePage />

    }
];



export default routes;