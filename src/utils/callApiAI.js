import axios from 'axios';
import * as Config from './../constants/Config';

export default function callApiAI(endpoint, method = 'GET', body){
    return axios({
        method: method,
        url: `${Config.API_URL_AI}/${endpoint}`,
        data: body
    })
    // .catch(err => {
    //     console.log(err.response.data);
    // });
};