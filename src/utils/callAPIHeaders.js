import axios from 'axios';
import * as Config from './../constants/Config';

export default function callApiHeder(endpoint, method = 'GET', body,token){
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        data: body,
        headers: {
            Authorization: 'Bearer ' + token 
          }
    })
   
};