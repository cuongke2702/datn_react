import React, { Component } from 'react';
import RatingProduct from '../RatingProduct/RatingProduct';

class ProductComment extends Component {
    render() {
        return (
            <div>
                 {/* phần mô tả chi tiết sản phẩm */}
      <div className="container-description col-lg-10 col-md-10 col-sm-12 col-12">
      
        <div className="bar-description mx-auto">
          <ul className="list-description">
            <li className="description-item col-lg-4 col-md-4 col-sm-4 col-4" style={{borderRight: '3px solid orange'}}>
              Mô tả sản phẩm
            </li>
            <li className="description-item col-lg-4 col-md-4 col-sm-4 col-4">
              Thông số sản phẩm
            </li>
            <li className="description-item description-item-selected col-lg-4 col-md-4 col-sm-4 col-4" style={{borderLeft: '3px solid orange'}}>
              Đánh giá và hỏi đáp
            </li>
          </ul>
        </div>
      
        <div className="description-content ">
          <ul className=" ul-item mx-auto">
         
            <li className="li-item">
              <div className="tab-description">
                <div className="description-product">
                  {/* nội dung bên trong thẻ A */}
                </div>
              </div>
            </li>
          
            <li className="li-item">
              <div className="tab-description">
                <div className="attribute-product">
                  <table className="table mx-auto">
                    <thead className="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Thuộc tính</th>
                        <th scope="col">Thông số</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>dài x rộng x cao</td>
                        <td>20x30x40</td>							
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>chất liệu</td>
                        <td>thép</td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>tải trọng</td>
                        <td>25 kg</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </li>
          
            <li className="li-item">
              <div className="tab-description  show-tab">
                <div className="comment-product">
                 
                  <div className="bar-detail-rating mx-auto">
                    <div className="left">
                      <div className="mx-auto" style={{width: '90%'}}>
                        <div className="score">
                          <h1>4/5</h1>
                        </div>
                        <div className="stars" style={{height: '50px', fontSize: '200%'}}>
                          <ul>
                            <li>
                              <i className="fa fa-star star-select" />
                            </li>
                            <li>
                              <i className="fa fa-star star-select" />
                            </li>
                            <li>
                              <i className="fa fa-star star-select" />
                            </li>
                            <li>
                              <i className="fa fa-star star-select" />
                            </li>
                            <li>
                              <i className="fa fa-star" />
                            </li>
                          </ul>
                        </div>
                        <div className="count">
                          <h6>20 lượt đánh giá</h6>
                        </div>
                      </div>
                    </div>
                    <div className="right">
                      <table>
                        <tbody>
                          {/* dòng 1 sao  */}
                          <tr>
                            <td>
                              <div className="stars" style={{width: '100px'}}>
                                <ul>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star " />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td>
                              <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '10%'}} />
                              </div>
                            </td>
                            <td>
                              <span>
                                <h6>2</h6>
                              </span>
                            </td>
                          </tr>
                          {/* dòng 2 sao */}
                          <tr>
                            <td>
                              <div className="stars" style={{width: '100px'}}>
                                <ul>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select " />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td>
                              <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '15%'}} />
                              </div>
                            </td>
                            <td>
                              <span>
                                <h6>2</h6>
                              </span>
                            </td>
                          </tr>
                          {/* dòng 3 sao  */}
                          <tr>
                            <td>
                              <div className="stars" style={{width: '100px'}}>
                                <ul>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td>
                              <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '25%'}} />
                              </div>
                            </td>
                            <td>
                              <span>
                                <h6>2</h6>
                              </span>
                            </td>
                          </tr>
                          {/* dòng 4 sao */}
                          <tr>
                            <td>
                              <div className="stars" style={{width: '100px'}}>
                                <ul>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star" />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td>
                              <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '30%'}} />
                              </div>
                            </td>
                            <td>
                              <span>
                                <h6>2</h6>
                              </span>
                            </td>
                          </tr>
                          {/* dòng 5 sao */}
                          <tr>
                            <td>
                              <div className="stars" style={{width: '100px'}}>
                                <ul>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                  <li>
                                    <i className="fa fa-star star-select" />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td>
                              <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '20%'}} />
                              </div>
                            </td>
                            <td>
                              <span>
                                <h6>2</h6>
                              </span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

               <RatingProduct/>
                
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
            </div>
        );
    }
}

export default ProductComment;