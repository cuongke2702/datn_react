import React, { Component } from "react";
import { Link } from "react-router-dom";
import jwt_decode from "jwt-decode";
import Loginfile from "./loginfile";
import { connect } from "react-redux";
import { logouUserFectAPI, searchByNameAPI, getCateAdminCallApi } from '../../actions';
import { Redirect } from 'react-router-dom';
import Logoutfile from "./logoutFile";

class Menu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      numCarts: '',
      isLoggedIn: {},
      txtName: '',
      showAccountMenu: false
    };
  }

  componentDidMount() {
    this.setState({
      numCarts: JSON.parse(localStorage.getItem('cart'))?.length
    })
    this.props.fetchAPI();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.cart !== this.props.cart) {
      const { cart } = this.props;
      this.setState({
        numCarts: cart.length
      });
    }
    if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
      const { isLoggedIn } = this.props;
      this.setState({
        isLoggedIn: isLoggedIn
      });
    }
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  };
  toggleFunction =()=> {
    console.log('heelo')
  
    this.setState({
      showAccountMenu: !this.state.showAccountMenu,
    });
  }

  onSearch = (txtName) => {
    this.props.onSearchByName(txtName);
    return <Redirect to="/home"/>;
  }
  render() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { txtName } = this.state
    const { categories } = this.props
    var decoded = null;
    if (user !== null) {
      decoded = jwt_decode(user.token);
    }
    const numCarts = JSON.parse(localStorage.getItem('cart'))?.length
    return (
      <div>
        <div className="header">
          <div className="bar-search">
            <div className="logo" id="id-logo">
              <a href="/home" className="home-link" title="Trang chủ">
                <img src="/images/logo.png" width="100%" style={{ objectFit: 'contain', objectPosition: 'center', height: '70px' }} />{" "}
              </a>
            </div>
            <div className="search-blank  ">
              <div className="child-search-blank mx-auto ">
                <input name="txtName" onChange={this.handleChange}
                  className="form-search"
                  type="search"
                  placeholder="Tìm kiếm trên VungCaoFood..."
                />
                <button className="btn-search " type="button" title="Tìm kiếm" onClick={() => this.onSearch(txtName)}>
                  <i className="fa fa-search" />
                </button>
              </div>
            </div>
            <span className="cart-box">
              <Link to={'/cart'}>
                <a href="cart.html">
                  <span className="cart" title="Giỏ hàng">
                    <i className="fa fa-shopping-cart" />
                  </span>
                  <span className="num-cart">{numCarts ? numCarts : 0}</span>
                </a>
              </Link>
            </span>
            <div className="options-menu">
              <div class="dropdown">
                <button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={this.toggleFunction}>
                  <i className="fa fa-user"></i>
                </button>
                <div className={'dropdown-menu dropdown-menu-right ' + ( this.state.showAccountMenu ? "show" : "hide" )} aria-labelledby="dropdownMenuButton">
                <ul className="navbar-nav  mx-auto">
                {/* <li className="nav-item">
                  <a className="nav-link" href="#">
                    Bán hàng
                  </a>
                </li> */}
                <li className="nav-item">
                  <a className="nav-link" href="/order">
                    Kiểm tra đơn hàng
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/profile">
                    Thông tin cá nhân
                  </a>
                </li>
                {
                  (decoded !== null) ?
                    <Logoutfile decoded={decoded} />
                    :
                    <Loginfile />
                }
                <li
                  className="nav-item"
                  style={{
                    visibility: "hidden",
                    opacity: 0,
                    zIndex: 1000,
                    position: "fixed",
                  }}
                >
                  {/* <a className="nav-link" href="#">
                    Thông tin cá nhân
                  </a> */}
                </li>
              </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="mini-menu">
            <div class="drop-down-menu">
              <p>
                DANH MỤC <i className='fa fa-angle-down'></i>
              </p>
              <div className="drop-down-content">
                {
                  categories.categories.map(data => (
                    <Link
                      to={`/product-byCat/${data.catId}`}
                    >
                      <h6 class="dropdown-item">{data.catName}</h6>
                    </Link>
                  ))
                }
              </div>
            </div>
            <div className="mini-sevice-content ">
              <div className="content mx-auto">
                <div className="mini-card-service" title="Liên hệ">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-shopping-bag" />
                    </span>
                    <span className="title-card">Hàng chính hãng</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Đánh giá">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-star" />
                    </span>
                    <span className="title-card">Đánh giá cao</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Mã giảm giá">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-bookmark" />
                    </span>
                    <span className="title-card">Mã giảm giá</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Ưu đãi">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-heart" />
                    </span>
                    <span className="title-card">Ưu đãi</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.users,
    cart: state.cart,
    categories: state
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onLogout: () => {
      dispatch(logouUserFectAPI())
    },
    onSearchByName: (txtName) => {
      dispatch(searchByNameAPI(txtName))
    },
    fetchAPI: () => {
      dispatch(getCateAdminCallApi())
    },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Menu);
