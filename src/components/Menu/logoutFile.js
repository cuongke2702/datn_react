import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { logouUserFectAPI } from '../../actions';


class Logoutfile extends Component{

    logout=()=>{
        this.props.onLogout();
      }
    render(){
        const {decoded}=this.props;
        return (
            <span>
                <ul className="navbar-nav  mx-auto">
                  <li className="nav-item">
                    <a className="nav-link"    >
                     {decoded.sub}
                    </a>
                  </li>
                  <Link to={`/auth/login`}>
                   <li className="nav-item">
                     <a className="nav-link"  onClick={this.logout}  >
                       Đăng xuất
                     </a>
                   </li>
                   </Link>
                </ul>
            </span>
        )
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onLogout : () =>{
            dispatch(logouUserFectAPI())
        }
    };
  };
export default connect(null,mapDispatchToProps)(Logoutfile);