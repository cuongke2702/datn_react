import React, { Component } from 'react';
import { connect } from 'react-redux';
import CheckOutAction from '../../pages/CheckOutActionPage/CheckOutAction';
import findProduct from '../../utils/findProduct';
import FormatCash from '../../utils/formatCurrency';

class CheckOutDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      product: {},
    };
  }

  componentDidMount() {
    const { cart, match } = this.props;
    let product = null;
    if (match) {
      const { cartId } = match.match.params;
      product = JSON.parse(localStorage.getItem('buy'));
      if (!cart || !cart.length || product) {
        this.setState({
          product: product,
        })
      } else {
        product = cart.find(each => each.cartId.toString() === cartId.toString())
        this.setState({
          product: product,
        })
      }

    }

  }
  render() {
    let { product } = this.state;
    return (
      <div>
        <div className="package">
          <div className="title-packege" style={{ backgroundColor: 'orange', marginBottom: '10px', width: '100%', display: 'inline-block' }}>
            <div className="title-package-left float-left" style={{ fontSize: '120%', fontWeight: 'bold' }}>
              Thông tin sản phẩm
              </div>
            <div className="title-package-right float-right">
              <span style={{ marginRight: '5px' }}>được giao bởi:</span><span style={{ fontWeight: 'bold', color: 'white', marginRight: '5px' }}>olala shop</span>
            </div>
          </div>
          <div className="content-package">
            <div className="col-product-left">
              <div className="infor-product">
                <div className="img-product" style={{ backgroundImage: 'url(images/tao-do.jpg)' }} />
                <div className="infor-product-right">
                  <div className="name">
                    {product ? product.productName : ""}
                  </div>
                  <div className="list-properties-product">
                  </div>
                  <div className="qty-product">
                    <span style={{ marginRight: '10px, margin-bottom: 20px', marginTop: '20px' }}>số lượng: </span><span style={{ color: 'orange', fontWeight: 'bold' }}>{product ? product.quantity : ""}</span>
                  </div>
                  <div className="icon-ship">
                    <i className="fa fa-car" />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-product-price">
              <div className="price">
                <h3 style={{ color: 'orange' }}><FormatCash str={String(product?.quantity * product?.price) || '000'} /><span style={{ textDecorationLine: 'underline', marginLeft: '2px' }}>đ</span></h3>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};


export default connect(mapStateToProps, null)(CheckOutDetail);