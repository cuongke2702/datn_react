import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import CartActionUpdate from '../../pages/CartActionPage/CartActionUpdate';
import FormatCash from '../../utils/formatCurrency';
class Cart extends Component {

  constructor(props) {
    super(props);
    this.setState({
      cart: null,
    })
  }
  componentDidMount() {
    this.componentWillReceiveProps();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.cart) {
      const { cart } = nextProps;
      this.setState({
        cart: cart
      })
    }
  }

  handleDelete = (product) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        const cart = localStorage.getItem('cart');
        const oldCart = JSON.parse(cart);
        const newCart = oldCart.filter((item) => item.productId !== product.productId);
        localStorage.setItem('cart', JSON.stringify(newCart));
        window.location.reload();
        Swal.fire(
          'Deleted!',
          'Your product has been deleted.',
          'success'
        )
      }
    })

  }

  Payment = () => {

  }

  render() {
    const { cart } = this.props;
    console.log(cart);
    return (
      <div className="list-product-of-cart">
        <div className="list-content mx-auto">
          <div className="title"><h5>GIỎ HÀNG</h5></div>
          <ul>
            {
              cart ? cart.map((product, index) => (
                <li key={index} id={1 + 1}>
                  <div className="item-cart" idproductincart={3}>
                    <div className="infor-mall">
                      <div className="skin">
                        <div className="name-of-mall">olala shop</div>
                      </div>
                      <div className="btn-delete-product-from-cart">
                        <button title="xóa sản phẩm" onClick={() => this.handleDelete(product)} ><i className="fa fa-trash" /></button>
                      </div>
                    </div>
                    <img className="img-product" style={{ backgroundImage: `../../images/` + product.image }} src={product.image}>
                    </img>
                    <div className="quick-infor">
                      <div className="name">{ }</div>
                      <div className="code-color-selected">
                        <span className="label">Tên sản phẩm:</span><span className="value" idcolor="123abc">{product.productName}</span>
                      </div>
                      <div className="size-selected">
                        <span className="label">Giá:</span><span className="value" idsize="10 lít"><FormatCash str={String(product.price)} /> đ</span>
                      </div>
                      <div className="qty-selected">
                        <span className="label">Số lượng:</span><span className="value" qtyproduct={1}>{product.quantity}</span>
                      </div>
                      <div className="bar-button-feedback">
                        <span>
                          <button>
                            <i className="fa fa-share" />
                          </button>
                        </span>
                        <span>
                          <button>
                            <i className="fa fa-heart" />
                          </button>
                        </span>
                      </div>
                    </div>
                    <div className="price-product">
                      <div className="price">
                        <span><FormatCash str={String(product.quantity * product.price)} /></span><span style={{ textDecorationLine: 'underline' }}>đ</span>
                      </div>
                    </div>
                    <div className="col-btn-right">
                      <div className="btn-change-option-product">
                        {/* <button>CHỈNH SỬA</button> */}
                      </div>
                      <div className="btn-buy-product">
                        <Link onClick={() => localStorage.removeItem('buy')} to={`/checkout/${product.cartId}`}><button>THANH TOÁN</button></Link>
                      </div>
                    </div>
                  </div>
                </li>
              )) : <>gfhgchgchg</>
            }
            <CartActionUpdate />

          </ul>
        </div>
      </div>

    );
  }
}
const mapStateToProps = state => {
  return {
    cart: state.cart
  }
}
const mapDispatchToProps = () => {

}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);