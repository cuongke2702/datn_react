import React, { Component } from 'react';

import { connect } from "react-redux";
import { actAddToCart, actGetProductsByCatIdRequest } from "./../../actions/index";
import ProductItem from './ProductItem';

class ProductListByCat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products:[]
    };
  }
  componentDidMount() {
    var { match } = this.props;
        if (match) {
          var catId = match.params.catId;
          this.props.onGetProductByCat(catId);
        }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.productsByCat !== this.props.productsByCat) {
      const { productsByCat } = this.props;
      this.setState({
        products:productsByCat
      });
    }
    if( prevProps.match.params.catId !== this.props.match.params.catId){
      this.props.onGetProductByCat(this.props.match.params.catId);
    }
  }
  render() {
    const { products } = this.state;
    console.log(products);
    return (
      <div>
        <div>
          <div className="list-result-product" id="section-4">
            <div className="title">
              
              <span />
            Danh sách sản phẩm<span style={{ marginLeft: "5px" }}></span>
            </div>
            <div className="content mx-auto">
              {
                products.map((product, index) => (
                  <ProductItem key={index} product={product}  /> 
                ))
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productsByCat: state.products,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetProductByCat: (catId) => {
      dispatch(actGetProductsByCatIdRequest(catId));
    },
    onAddToCart: (product) => {
      dispatch(actAddToCart(product, 1));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductListByCat);
