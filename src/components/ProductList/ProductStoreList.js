import React, { Component } from "react";
import { connect } from "react-redux";
import { actAddToCart, actFetchProductsRequest,actGetProductsByCatIdRequest } from "./../../actions/index";
import ProductItem from "./ProductItem";
import ProductList from "./ProductList";

class ProductStoreList extends Component {
  componentDidMount() {
    this.props.fetchAllProducts();
  }
  render() {
    var { productsByCat } = this.props;
    return <div>{this.showStaffs(productsByCat)}</div>;
  }

  showStaffs(productsByCat) {
    var { onAddToCart } = this.props;
    var result = null;
    if (productsByCat.length > 0) {
      result = productsByCat.map((product, index) => {
        return (
          <>
          <ProductItem key={index}
           product={product} onAddToCart = {onAddToCart} />
           </>
        );
      });
    }
    return result;
  }
  
}

const mapStateToProps = (state) => {
  return {
    productsByCat: state.products,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllProducts: () => {
      dispatch(actGetProductsByCatIdRequest());
    },
    onAddToCart: (product) => {
      dispatch(actAddToCart(product, 1));
  }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductStoreList);
