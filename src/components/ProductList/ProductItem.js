import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FormatCash from '../../utils/formatCurrency';

class ProductItem extends Component {
  render() {
    var { product, onClick } = this.props;
    var rateAvgFlorr=Math.floor(product.rateAvg)
    return (
      <div>
        <Link onClick={onClick} to={`/product-detail/${product.productId}`} style={{ color: "black" }}>
          <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6"  title={product.productName}>
            <div className="content-product">
              <div
                className="img-product-content"
              >
                <img
                  src={`${product.images}`}
                />
              </div>
              <div className="name-product">
                <h5 className='text-maxlength'>{product.productName}</h5>
              </div>
              <div className="price-product" style={{ color: "#ff8100" }}>
                <h3><FormatCash str={String(product.price)} /> đ</h3>
              </div>
              <div className="stars">
                  <ul>
                    {
                      Array.from(Array(rateAvgFlorr), (e, i) => {
                        return <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      })
                    }
                     {
                      Array.from(Array(5-rateAvgFlorr), (e, i) => {
                        return <li>
                        <i className="list-star-item fa fa-star" />
                      </li>
                      })
                    }
                    
                  </ul>
                  <div className="total-vote">
                    <h6>({product.quantity})</h6>
                  </div>
                </div>
            </div>
          </div>
        </Link>


      </div>
    );
  }
  onAddToCart = (product) => {
    this.props.onAddToCart(product);
  }

}

export default ProductItem;