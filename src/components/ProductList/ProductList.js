import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ProductList extends Component {
    render() {
      const {product} = this.props
     
        return (
            <div>
            <Link to={`/product-detail/${product.productId}`} style={{ color: "black" }}>
              <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
                <div className="content-product">
                  <div
                    className="img-product-content"
                  >
                    <img
                      src={product.images}
                    />
                  </div>
                  <div className="name-product">
                    <h5>{product.productName}</h5>
                  </div>
                  <div className="price-product" style={{ color: "#ff8100" }}>
                    <h3>{product.price}</h3>
                  </div>
                  <div className="stars">
                    <ul>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star" />
                      </li>
                    </ul>
                    <div className="total-vote">
                      <h6>(71)</h6>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
         
             
            </div>
        );
    }
}

export default ProductList;
