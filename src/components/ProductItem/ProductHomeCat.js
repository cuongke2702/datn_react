import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getCateAdminCallApi} from '../../actions';

class ProductHomeCat extends Component {

  componentDidMount() {
    this.props.fetchAPI();
  }
    render() {
      const {categories} =this.props
        return (
           <div>
                 {/* tab-list */}
        <div className="dropdown-sub-menu" dropmenupoisition="ab">
          <ul className="vcf-list dropdown-sub-menu-level-1">
            
            {
              categories.categories.map(data =>(
                <Link to={`/product-byCat/${data.catId}`}>
                <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={1}>
                <h6>{data.catName}</h6>
                </li>
            </Link>
              ))
            }
           
          </ul>
        </div>
        
                {/* sản phẩm tìm kiếm phổ biến*/}
           </div>
        );
    }
}
const mapStateToProps = state => {
  return {
    categories: state
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPI: () => {
      dispatch(getCateAdminCallApi())
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductHomeCat);
