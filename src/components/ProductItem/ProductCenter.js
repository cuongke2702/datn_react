import React, { Component } from 'react';

class ProductCenter extends Component {
    render() {
        return (
           <div>
                 {/* trung tâm mua sắm */}
      <div className="vcf-mall-container" id="section-2">
        <div className="title">
          <span style={{marginRight: '5px'}}>--</span><span />Trung tâm mua sắm<span style={{marginLeft: '5px'}}>--</span>
        </div>
        <div className="content mx-auto">
          <div className="mall-slide-carousel">
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div status="hidden" className="mall-carousel-item active">
              <a href="mall.html">
                <div className="tag-vcf-mall">
                  <div className="tag-vcf-mall-content">
                    <div className="logo-vcf-mall mx-auto">
                      <div className="img-logo" style={{backgroundImage: 'url(images/vcf.jpg)'}}>
                      </div>
                    </div>
                    <div className="name-vcf-mall">
                      Olala shop
                    </div>
                    <div className="num-type-product-in-mall">
                      <span className="num">1.000</span><span style={{marginLeft: '5px'}}>sản phẩm</span>
                    </div>
                    <div className="vote-star">
                      <div className="stars mx-auto">
                        <ul>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star star-select" />
                          </li>
                          <li>
                            <i className="list-star-item fa fa-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
             </div>
        );
    }
}

export default ProductCenter;
