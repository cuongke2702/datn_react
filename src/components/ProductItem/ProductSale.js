import React, { Component } from "react";
import ProductList from "../ProductList/ProductList";
import { connect } from "react-redux";
import { actAddToCart, actFetchPopularProductsRequest, actFetchProductsRequest } from "./../../actions/index";
import ProductItem from "../ProductList/ProductItem";

class ProductSale extends Component {

  componentDidMount() {
    this.props.fetchPopularProducts();
    this.props.fetchAllProducts();
    
  }
  
  render() {
    var { products, popularProducts } = this.props;
    return (
      <div>
        <div className="list-result-product" id="section-4">
          <div className="title">
            <span>
            Sản phẩm bán chạy nhất</span>
          </div>
          <div className="content mx-auto">
             {
              popularProducts.map((product, index) => (
                <ProductItem key={index} product={product}  /> 
               ))
             }
          </div>
        </div>
        {/* sản phẩm ưu đãi khuyến mãi */}
        <div className="list-result-product" id="section-4">
          <div className="title">
            <span>
            Danh sách sản phẩm</span>
          </div>
          <div className="content mx-auto">
             {
               products.map((product, index) => (
                <ProductItem key={index} product={product}  /> 
               ))
             }
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    products: state.products,
    popularProducts: state.popularProducts,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllProducts: () => {
      dispatch(actFetchProductsRequest());
    },
    fetchPopularProducts: () => {
      dispatch(actFetchPopularProductsRequest());
    },
    onAddToCart: (product) => {
      dispatch(actAddToCart(product, 1));
  }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductSale);
