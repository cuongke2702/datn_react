import * as Types from '../constants/ActionTypes';

const user = JSON.parse(localStorage.getItem("user"));

var initialState = user
? { isLoggedIn: true, user }
: { isLoggedIn: false, user: null };

const users = (state = initialState, action) => {
    switch(action.type){
        case Types.LOGIN:
            return {
                ...state,
                isLoggedIn: true,
                user:action.user
              };
        case Types.LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                user:null,
                };
        default:
            return state;
    }
}

export default users;
