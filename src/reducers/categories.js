import * as Types from "../constants/ActionTypes";

const initialState = [];

const categories = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_CATEGOGY: {
      state = [...action.cat]
      return [...state];
    }
    default:
      return [...state];
  }
}

export default categories;