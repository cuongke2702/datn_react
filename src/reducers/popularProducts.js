import * as Types from "../constants/ActionTypes";
var initialState = [];

const popularProducts = (state = initialState, action) => {
    
  switch (action.type) {
    case Types.FETCH_POPULAR_PRODUCT:
      state = action.popularProducts;
      return [...state];
    default: return [...state];
  }
};

export default popularProducts;