import { combineReducers } from 'redux';
import products from './products';
import itemEditing from './itemEditing';
import cart from './cart';
import comments from './comments';
import register from './register';
import users from './users';
import categories from './categories';
import productsByCat from './productsByCat';
import order from './order';
import popularProducts from './popularProducts'

const appReducers = combineReducers({
    products,
    itemEditing,
    cart,
    comments,
    register,
    users,
    categories,
    productsByCat,
    order,
    popularProducts,
});

export default appReducers;