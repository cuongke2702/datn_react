import * as Types from "../constants/ActionTypes";
var initialState = [];

const productsByCat = (state = initialState, action) => {
   
  switch (action.type) {
      case Types.GET_PRODUCT_BY_CAT:
        state = action.products;
        return [...state];
    default: return [...state];
  }
};

export default productsByCat;
