import * as Types from "../constants/ActionTypes";
var initialState = [];

const order = (state = initialState, action) => {

  switch (action.type) {
    case Types.GET_ORDER:
      state = action.order;
      return [...state];
    case Types.UPDATE_ORDER:
      // state = action.order;
      return [...state];
    default: return [...state];
  }
};

export default order;
